gRoot = getRootElement()

settings = {}
settings[1] = 0 -- drawTime (if 0 then depends on playerCount)
settings[2] = 20 -- +- rocks amount per map/ decoration 
settings[3] = 3 -- +- trees on each rock
settings[4] = 2 -- +- markers on each rock
settings[5] = 3 -- +- palms trees per user
settings[6] = 20 -- luxorpillars


Trees = {3505,3506,3507,3508,3509,3510,3511,3512}
Rocks = {18228,17034,17031}
RealTrees = {726,727,729,730,732,733,734,776,3517}
Luxor = 8397
Map={}

addEventHandler( "onResourceStart", getResourceRootElement(getThisResource()),
	function ()	
	
	
	
		outputChatBox(tostring(hasObjectPermissionTo (getThisResource(),"general.ModifyOtherObjects" , false)))
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		--time check
		if settings[1] == 0 then
			if getPlayerCount() > 0 then
				settings[1] = 30 - 3*math.floor(getPlayerCount()/4)
			else
				settings[1] = 30
			end
		end
		
		setWaterColor(255,255,255 , 255)
		
		local lotOfRed1 = math.random(0,1)
		local lotOfGreen1 = math.random(0,1)
		local lotOfBlue1 = math.random(0,1)
		local lotOfRed2 = math.random(0,1)
		local lotOfGreen2 = math.random(0,1)
		local lotOfBlue2 = math.random(0,1)
		setSkyGradient( math.random(0,100)+lotOfRed1*150, math.random(0,100)+lotOfGreen1*150 , math.random(0,100)+lotOfBlue1*150, math.random(0,100)+lotOfRed2*150, math.random(0,100)+lotOfGreen2*150 , math.random(0,100)+lotOfBlue2*150 )
	
		--invisible airportlands
		for x=-12,12 do
			for y=-4,4 do
				local o = createObject(8171,5000+x*40,0+y*100,1.1)
				setElementInterior(o,1)
			end
		end
	
		-- Creating decorations
		for i=1,settings[2] do
			
			local x,y
			repeat x,y = 5000+math.random(-600,600),math.random(-600,600) until getDistanceBetweenPoints2D(x,y,5000,0) > 200
			createObject(Rocks[math.random(#Rocks)],x,y,-5,0,0,math.random(0,360))
			createObject(Rocks[math.random(#Rocks)],x,y,-5,0,0,math.random(0,360))
			for i=1,settings[3] + math.random(-2,2) do
				createObject(RealTrees[math.random(#RealTrees)],x+math.random(-15,15),y+math.random(-15,15),0,0,0,math.random(0,360))
			end
			for i=1,settings[4] + math.random(2,4) do
				createMarker(x+math.random(-15,15),y+math.random(-15,15),math.random(0,15),"corona",15,math.random(0,255),math.random(0,255),math.random(0,255))
			end
			
			--AntiCamp/Stuck carshade
			
			for i=0,5 do
				local nx,ny = getPointFromDistanceRotation(x,y,32,i*60)
				createObject(1940,nx,ny,-1.9+i*0.0005,0,0,-i*60)
			end
		end
		for i=1,settings[6] do
			local x,y
			repeat x,y = 5000+math.random(-600,600),math.random(-600,600) until getDistanceBetweenPoints2D(x,y,5000,0) > 100
			for i=1,5 do
				createObject(Luxor,x,y,-15,0,math.random(-25,25),math.random(0,360))
			end
		end
		
		rotatemoveLuxor()
	end 
)

addEvent("onRaceStateChanging",true)
addEventHandler('onRaceStateChanging', gRoot,
	function(stateName,currentRaceStateName)
		if stateName == "MidMapVote" then
			return
		elseif stateName == "Running" and currentRaceStateName == "GridCountdown" then
			drawingStart()
		end
	end
)

function drawingStart()
	triggerClientEvent(gRoot,"drawingStart",gRoot,settings[1])
	setTimer(drawingStop,settings[1]*1000,1)
end

function drawingStop()
	triggerClientEvent(gRoot,"drawingStop",gRoot,settings[5])
	for i,theObj in ipairs(getElementsByType("object")) do
		if getElementModel(theObj) == 8171 then
			destroyElement(theObj)
		end
	end
	for i,thePlayer in ipairs(getElementsByType("player")) do
		local car = getPedOccupiedVehicle(thePlayer)
		if isElement(car) then
			local x,y = getElementPosition(car)
			local o = createObject(13649,x,y,-1,0,0,0)
			moveObject(o,1000,x,y,0.55)
		end
	end
end

function createCarShade(x,y,r,myObjCount)
	local z = math.mod(myObjCount,3)/100
	createObject(1940,x,y,-1.9+z,0,0,r)
end
addEvent("createCarShade",true)
addEventHandler('createCarShade', gRoot,createCarShade)

function createPalm(x,y)
	local p = createObject(Trees[math.random(#Trees)],x,y,-15)
	local r = createObject(744,x,y,-16)
	attachElements(r,p,0,0,-3)
	moveObject(p,math.random(5000,15000),x,y,2,0,0,math.random(-360,360))
end
addEvent("createPalm",true)
addEventHandler('createPalm', gRoot,createPalm)

addEvent("onClientReady",true)
addEventHandler('onClientReady', gRoot,
	function()
		bindKey (source,"horn","both", HornBind)
	end
)


function HornBind(playerSource,key,keystate)
	if playerSource then
		local car = getPedOccupiedVehicle(playerSource)
		if car then
			if getPedOccupiedVehicleSeat ( playerSource ) == 0 then
				for i=0,3 do
					setVehicleLightState ( car, i, 0)
				end
				if keystate == "down" then
					setVehicleOverrideLights ( car, 1 )
				else
					setVehicleOverrideLights ( car, 2 )
				end
			end
		end
	end
end

function rotatemoveLuxor()
	for i,theObject in ipairs(getElementsByType("object")) do
		if getElementModel(theObject) == Luxor then
			local x,y,z = getElementPosition(theObject)
			moveObject(theObject,30000,x+math.random(-5,5),y+math.random(-5,5),z+math.random(-5,5),math.random(-5,5),math.random(-5,5),math.random(0,360))
		end
	end
	setTimer(rotatemoveLuxor,30000,1)
end

---------------------------------------------------------------------------------------
--------------------------- UTILS -----------------------------------------------------
---------------------------------------------------------------------------------------


function getPointFromDistanceRotation(x, y, dist, angle)
 
    local a = math.rad(90 - angle);
 
    local dx = math.cos(a) * dist;
    local dy = math.sin(a) * dist;
 
    return x+dx, y+dy;
 
end