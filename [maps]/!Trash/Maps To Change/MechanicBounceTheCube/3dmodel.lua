addEventHandler('onClientResourceStart', resourceRoot,
    function()
 
        local txd = engineLoadTXD('Files/3dmodel.txd',true)
        engineImportTXD(txd, 8838)
 
        local dff = engineLoadDFF('Files/3dmodel.dff', 0)
        engineReplaceModel(dff, 8838)
 
        local col = engineLoadCOL('Files/3dmodel.col')
        engineReplaceCOL(col, 8838)
        engineSetModelLODDistance(8838, 0)

        local txd = engineLoadTXD('Files/3dmodel1.txd',true)
        engineImportTXD(txd, 8558)
 
        local dff = engineLoadDFF('Files/3dmodel1.dff', 0)
        engineReplaceModel(dff, 8558)
 
        local col = engineLoadCOL('Files/3dmodel1.col')
        engineReplaceCOL(col, 8558)
        engineSetModelLODDistance(8558, 0)

	end 
)
