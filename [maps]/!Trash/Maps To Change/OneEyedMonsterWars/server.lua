-- sasdfasd
-- fasd
-- fasd
-- fas
-- df
-- asdfasdfasdf


uniqueKey = tostring(getResourceName(getThisResource()))

function start(newstate,oldstate)
	if (newstate == "LoadingMap") then
		
		--Define global parameters
		startx,starty,startz = math.random(-1800,1800),math.random(-1800,1800),200
		
		local playerCount = getPlayerCount()
		gridSize = (math.ceil(math.sqrt(playerCount))+1)*3
		
		--Create arrays to store data
		grid = {}
		powerUpPositions = {}
		
		--Create a nestled loop to fill the arrays with data and create neon objects
		for a=0,gridSize do
			grid[a]={}
			powerUpPositions[a] = {}
			for b=0,gridSize do
				local neon = createObject(9126,startx+a*9,starty+b*9,startz-1.15)
				setObjectScale(neon,0.5)
				setElementRotation(neon,90,0,0)
				grid[a][b] = false
				powerUpPositions[a][b] = false
			end
		end
		
		--Create powerups and move them to a random point on the grid
		cowerup = createObject(16442,startx,starty,startz+2,0,0,230)
		movePowerup(cowerup,1)
		ufo = createObject(16778,startx,starty,startz-3,0,0,90)
		movePowerup(ufo,2)
		car = createObject(10281,startx,starty,startz+1.5,-90,0,0)
		movePowerup(car,3)
		
	end
	
	if (newstate == "GridCountdown") then
		outputChatBox("The Grid Size is set at "..gridSize.." x "..gridSize.."!",root,0,255,0)
		outputChatBox("Use arrow keys or wasd to change direction!",root,0,255,0)
		-- Create arrays to store player data
		players = {}
		playerDirections = {}
		playerHistory = {}
		playerSize = {}
		playerSpeed = {}
		speedTimer = {}
		
		--Spawn each player at a grid point
		spawnCounter = 0
		for i,p in ipairs (getElementsByType("player")) do
			local veh = getPedOccupiedVehicle(p)
			if veh then
				--pick a gridpoint for the player
				--gridSize = (math.ceil(math.sqrt(playerCount))+1)*3
				local x = 1+(spawnCounter*3)%gridSize
				local y = 1+math.floor((spawnCounter+1)/(gridSize/3))*3
				setElementPosition(veh,startx+x*9,starty+y*9,startz)
				grid[x][y] = true
				
				--Create a tile that moves with the player, and attach the player vehicle to it
				local newTile = createObject(3095,startx+x*9,starty+y*9,startz)
				setElementCollisionsEnabled(newTile,false)
				attachElements(veh,newTile,0,0,2)
				
				--Update the arrays with the information of the player
				players[p] = {newTile,x,y}
				playerDirections[p]={0,1}
				playerSize[p] = 2
				playerSpeed[p] = 2 --player speed is this number multiplied by 100 ms
				
				--Create a tile that stays after the player moves and add it to the history
				playerHistory[p]={}
				addToHistory(p,x,y)	
			end
			spawnCounter = spawnCounter +1 --add one to the spawn counter so the next player doesn't spawn in the same spot
			
			-- Bind keys to players can pick a direction to go in
			bindKey(p,"vehicle_right","down",changeTileDirection,1,0) 	--right
			bindKey(p,"vehicle_left","down",changeTileDirection,-1,0)	--left
			bindKey(p,"accelerate","down",changeTileDirection,0,1)		--up
			bindKey(p,"brake_reverse","down",changeTileDirection,0,-1)	--down
			bindKey(p,"steer_forward","down",changeTileDirection,0,1)	--up
			bindKey(p,"steer_back","down",changeTileDirection,0,-1)		--down
		end
		triggerClientEvent("updateGrid",root,grid,powerUpPositions)		
	end	


	if (newstate == "Running" and oldstate == "GridCountdown") then
		--Creating a function that loops itself every 100 ms
		moveTime = 250
		tickCounter = 0
		setTimer(function()
			for index,p in pairs (getAlivePlayers()) do --loop through all alive players
				if (playerSpeed[p] ~= 0) then
				if (tickCounter%playerSpeed[p] == 0) then --see if the player will move this loop
					
					if playerDirections[p] and players[p] then
					--Deleting end of the snake:
					if #playerHistory[p] > playerSize[p] then
						local tile,x,y = unpack(playerHistory[p][#playerHistory[p]]) --get the tile at the end of the table
						--Destroy the tile, remove it from the table and set the gridpoint to unoccupied
						destroyElement(tile)
						table.remove(playerHistory[p])
						grid[x][y]=false
					end
					
					local xmove,ymove = unpack(playerDirections[p]) --unpack the player direction
					local tile,x,y= unpack(players[p])	--unpack the playerTile and current position
					
					if ((x+xmove) <= gridSize) and ((x+xmove) >= 0) and ((y+ymove) <= gridSize) and ((y+ymove) >= 0)then --see if the player moves to a point inside the grid
						if not grid[x+xmove][y+ymove] then --see if that gridpoint is free
							players[p]={tile,x+xmove,y+ymove}	--update the player position
							addToHistory(p,x,y)
							
							--Move the player to the next gridPoint
							moveObject(tile,moveTime*playerSpeed[p],startx+(x+xmove)*9,starty+(y+ymove)*9,startz)
							--update the grid to indicate the space is taken
							grid[x+xmove][y+ymove] = true
							--landing in a powerup:
							if powerUpPositions[x+xmove][y+ymove] then
								
								if powerUpPositions[x+xmove][y+ymove] == 1 then -- if the powerup is of type 1
									playerSize[p] = playerSize[p]+1 --increase size of player
									
									movePowerup(cowerup,1)
								end
								
								if powerUpPositions[x+xmove][y+ymove] == 2 then -- if the powerup is of type 1
									playerSpeed[p] = 0
									if isTimer(speedTimer[p]) then
										killTimer(speedTimer[p])
									end
									setTimer(function()
											playerSpeed[p]=2
											local veh = getPedOccupiedVehicle(p)
											if veh then
												setElementModel(veh,444)
											end
									end,17*moveTime,1)
									movePowerup(ufo,2)
									
									local sparrow = createVehicle(469,startx+(x+xmove)*9,starty+(y+ymove)*9,startz)
									setElementVelocity(sparrow,0,0,0.8)
									
								end
								
								if powerUpPositions[x+xmove][y+ymove] == 3 then
									playerSpeed[p] = 1
									local veh = getPedOccupiedVehicle(p)
									if veh then
										setElementModel(veh,411)
									end
									if isTimer(speedTimer[p]) then
										resetTimer(speedTimer[p])
									else
										speedTimer[p] = setTimer(function()
											playerSpeed[p]=2
											local veh = getPedOccupiedVehicle(p)
											if veh then
												setElementModel(veh,444)
											end
										end,10*moveTime,1)
									end
									movePowerup(car,3)
								end
								powerUpPositions[x+xmove][y+ymove] = false
							end
							
							
						else
							killPlayer(p) --kill players moving to a taken gridpoint
						end
					else
						killPlayer(p)	--and kill players moving outside the grid
					end
					
					
				end
				end
				end
			end
			tickCounter = tickCounter+1
			triggerClientEvent("updateGrid",root,grid,powerUpPositions)
		end,moveTime,0)
		
	end
end
addEvent ( "onRaceStateChanging", true )
addEventHandler ( "onRaceStateChanging", getRootElement(), start )

function movePowerup(powerup,number)
	--Picking a new spot for the powerup
	
	local i,newX,newY = 0,0,0
	local x,y,z = getElementPosition(powerup)
	
	while true do
		newX,newY = math.random(0,gridSize),math.random(0,gridSize)
		if (not grid[newX][newY]) and (not powerUpPositions[newX][newY]) then
			setElementPosition(powerup,startx+newX*9,starty+newY*9,z)
			powerUpPositions[newX][newY] = number
			break
		end
		
		i=i+1
		if i > 200 then
			--outputChatBox("Found no free tile.")
			newX,newY = -100,100
			setElementPosition(powerup,startx+newX*9,starty+newY*9,z)
			break
		end
	end
	
	
	setElementPosition(powerup,startx+newX*9,starty+newY*9,z)
	powerUpPositions[newX][newY] = number
end

function addToHistory(player,x,y)
	local o = createObject(3095,startx+x*9,starty+y*9,startz)
	setElementCollisionsEnabled(o,false)
	table.insert(playerHistory[player],1,{o,x,y})
	--new tiles are added on the first index, so adding a new tile will push the rest down. Then when the table gets too large, tiles at the end are deleted.
end

function killPlayer(p)
	--outputChatBox("intersect, get killed",p)
	
	--get player vehicle, detach it from the tile, give it a velocity and blow it up
	local veh = getPedOccupiedVehicle(p)
	if veh then
		detachElements(veh)
		velx,vely = unpack(playerDirections[p])
		setElementVelocity(veh,velx/3,vely/3,0.5)
		setVehicleTurnVelocity(veh,math.random()/5,math.random()/5,math.random()/5)
		blowVehicle(veh)
	end
	
	-- create a skull at the death spot of the player
	local _,x,y = unpack(players[p])
	local skull = createObject(6865,startx+x*9,starty+y*9,startz+1,325,70,78)
	setElementCollisionsEnabled(skull,false)
	
	-- remove the entry of the player in the arrays
	playerDirections[p] = false
	playerHistory[p] = {}
	playerSize[p] = 0
end

function changeTileDirection(keyPresser,key,keyState,x,y)
	--update the table with the new direction
	playerDirections[keyPresser] = {x,y}
	
	--calculate the rotational offsets for the new direction
	local rotz = 0
	if y == -1 then
		rotz = 180
	elseif x == -1 then
		rotz = 90
	elseif x == 1 then
		rotz = -90
	end
	
	--set the new rotation offset
	local veh = getPedOccupiedVehicle(keyPresser)
	if veh then
		setElementAttachedOffsets(veh,0,0,2,0,0,rotz)
	end
end

