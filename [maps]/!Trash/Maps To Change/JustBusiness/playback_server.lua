g_Root = getRootElement()

addEvent"onMapStarting"

function loadGhost()
	-- Load the old ghost if there is one
	local ghost = xmlLoadFile("path.gst")
	
	if ghost then
				
		-- Construct a table
		local index = 0
		local node = xmlFindChild( ghost, "n", index )
		while (node) do
			local attributes = xmlNodeGetAttributes( node )
			local row = {}
			for k, v in pairs( attributes ) do
				row[k] = convert( v )
			end
			table.insert(recording, row )
			index = index + 1
			node = xmlFindChild( ghost, "n", index )
		end
		xmlUnloadFile( ghost )		
		return true
	end
	return false
end

function sendGhostData()
	triggerClientEvent(g_Root, "onClientGhostDataReceive2", g_Root, recording )
end

addEventHandler( "onMapStarting", g_Root,
	function()
		recording = {}
		loadGhost()
		sendGhostData()
	end
)

function convert( value )
	if tonumber( value ) ~= nil then
		return tonumber( value )
	else
		if tostring( value ) == "true" then
			return true
		elseif tostring( value ) == "false" then
			return false
		else
			return tostring( value )
		end
	end
end
