function collision (hitElement,force, bodyPart, x, y, z, nx, ny, nz)
	if (hitElement) then
		if ( source == getPedOccupiedVehicle(localPlayer) ) and (getElementType(hitElement) == "vehicle") then
			triggerServerEvent ( "collisionamplifier", root, source,hitElement)
		end
	end
end
addEventHandler("onClientVehicleCollision", root, collision)

--mass = 10000
screenWidth, screenHeight = guiGetScreenSize ( )
topspeed = 0
mass = 0
actualspeed = 0
red = 0
scale = 0
function calculatespeed()
if getPedOccupiedVehicle(localPlayer) then
	local speedx, speedy, speedz = getElementVelocity(getPedOccupiedVehicle(localPlayer))
	actualspeed = math.floor(((speedx^2 + speedy^2 + speedz^2)^(0.5))*180)
	mass = getVehicleHandling(getPedOccupiedVehicle(localPlayer)).mass


if (actualspeed>topspeed) then topspeed = actualspeed end
if (actualspeed>127.5) then
	red = 255
else
	red = actualspeed*2
end
if (actualspeed>300) then
 scale = 3
else
 scale = math.floor(actualspeed)/100
end

end

dxDrawText ("Top speed:  " .. topspeed .. " Km/h", 10,screenHeight/2-50, 0,0, tocolor ( 255, 0, 0, 255 ),2,"default")
dxDrawText ("Vehicle Mass:  " .. mass .. " Kg", 10,screenHeight/2+50, 0,0, tocolor ( 255, 0, 0, 255 ),2,"default")
dxDrawText ("Current speed: " .. actualspeed .. " Km/h", screenWidth, screenHeight-50, 0, 0, tocolor ( red, 255-red, 0, 255 ),1+scale,"default","center")
end
addEventHandler("onClientRender",root,calculatespeed)

setTimer(function()
triggerServerEvent("setTopSpeed",root,localPlayer,topspeed)
end,5000,0)
