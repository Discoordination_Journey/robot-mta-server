-------
outputChatBox("Use primary fire to place mines!",root,255,0,255)
-------





handlecollisions = {}
function collision (theVehicle1,theVehicle2)
	--outputChatBox("collision (this should be output twice)")
	if (handlecollisions[theVehicle1] == nil) then handlecollisions[theVehicle1] = "notbusy" end
	if (handlecollisions[theVehicle2] == nil) then handlecollisions[theVehicle2] = "notbusy" end
	
	if (handlecollisions[theVehicle1] == "notbusy") and (handlecollisions[theVehicle2] == "notbusy") then
		handlecollisions[theVehicle1] = "busy"
		handlecollisions[theVehicle2] = "busy"
		--outputChatBox("after check (this should be output once)")
		setTimer(function()
			handlecollisions[theVehicle1] = "notbusy"
			handlecollisions[theVehicle2] = "notbusy"
		end,300,1)
		
		local a,b,c = getElementVelocity(theVehicle1)
		local x,y,z = getElementVelocity(theVehicle2)
		setElementVelocity(theVehicle1,0,0,0)	
		
		table1 = getVehicleHandling(theVehicle1)
		table2 = getVehicleHandling(theVehicle2)
		--outputChatBox(table1.mass)
		--outputChatBox(table2.mass)
		mass1 = table1.mass
		mass2 = table2.mass
			
		--outputChatBox("Your Velocity: x = " .. a .. ",  y = " .. b .. ",  z = " .. c)
		--outputChatBox("Other objects Velocity: x = " .. x .. ",  y = " .. y .. ",  z = " .. z)
		local vx1 = (mass1-mass2)/(mass2+mass1)*a + (2*mass2/(mass2+mass1))*x
		local vy1 = (mass2-mass1)/(mass2+mass1)*b + (2*mass2/(mass2+mass1))*y
		local vz1 = (mass2-mass1)/(mass2+mass1)*c + (2*mass2/(mass2+mass1))*z

		local vx2 = (mass2-mass1)/(mass2+mass1)*x + (2*mass1/(mass2+mass1))*a
		local vy2 = (mass2-mass1)/(mass2+mass1)*y + (2*mass1/(mass2+mass1))*b
		local vz2 = (mass2-mass1)/(mass2+mass1)*z + (2*mass1/(mass2+mass1))*c

		setElementVelocity(theVehicle1,vx1,vy1,vz1)
		setElementVelocity(theVehicle2,vx2,vy2,vz2)
	end
end
addEvent("collisionamplifier",true)
addEventHandler("collisionamplifier",root,collision)

theZone = createColCircle ( -2230, 1819, 250 )
function leftZone ( hitElement, matchingDimension )
    --outputChatBox("vehicle has left the zone1")
	if getElementType ( hitElement ) == "vehicle" then
		--setElementPosition(hitElement,-2213.2998046875,1817.099609375,130)
		respawnVehicle(hitElement)
		setElementModel(hitElement,485)
		outputChatBox(getPlayerName(getVehicleOccupant(hitElement)) .. "#ff0000 scored a homerun!",root,0,0,0,true)
    end
end
addEventHandler ( "onColShapeLeave", theZone, leftZone )

function start(newstate,oldstate)
	if (newstate == "GridCountdown") then
		Highestspeed = textCreateDisplay()
		HighestspeedText = textCreateTextItem(" ",0.5,0.05,"medium",255,255,0,255,2,"center")
		textDisplayAddText (Highestspeed, HighestspeedText )

		mineUnavailable = textCreateDisplay()
		mineUnavailableText = textCreateTextItem("Mine Unavailable",0.01,0.5,"medium",255,0,0,255,2,"left")
		textDisplayAddText (mineUnavailable, mineUnavailableText )
		
		mineAvailable = textCreateDisplay()
		mineAvailableText = textCreateTextItem("Mine Ready",0.01,0.5,"medium",0,255,0,255,2,"left")
		textDisplayAddText (mineAvailable, mineAvailableText )
		
		for index, thePlayer in ipairs (getAlivePlayers()) do
			textDisplayAddObserver ( Highestspeed, thePlayer )
			textDisplayAddObserver ( mineAvailable, thePlayer )
			bindKey (thePlayer, "vehicle_fire", "down", funcInput )
		end
	end
	if (newstate == "Running" and oldstate == "GridCountdown") then
		for index, theVehicle in ipairs (getElementsByType("vehicle")) do
			if (getVehicleOccupant(theVehicle)) then
				--outputChatBox("vehicle damageproof")
				setVehicleDamageProof(theVehicle,true)
			end
		end
	end
end
addEvent ( "onRaceStateChanging", true )
addEventHandler ( "onRaceStateChanging", getRootElement(), start )

topspeed = 0
function topspeedupdater(triggeredby,speed)
if (speed>topspeed) then
	topspeed = speed
	textItemSetText(HighestspeedText,"Overall Top Speed: " .. speed .. " Km/h By: " .. removeColorCodes(getPlayerName(triggeredby)))
end
end
addEvent("setTopSpeed",true)
addEventHandler("setTopSpeed",root,topspeedupdater)

function removeColorCodes(str)
  return (string.gsub(str, "#[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]", ""))
end

mine = {}
mineTimer = {}
function funcInput ( player, key, keyState )
	if isTimer(mineTimer[player]) then -- Check if timer is running using isTimer
		cancelEvent() 
	else	
		mineTimer[player] = setTimer(function(player) 
			mineTimer[player] = nil 
			textDisplayRemoveObserver ( mineUnavailable, player )
			textDisplayAddObserver ( mineAvailable, player )
		end, 15000, 1, player)
		
		textDisplayRemoveObserver ( mineAvailable, player )
		textDisplayAddObserver ( mineUnavailable, player )
		if (getPedOccupiedVehicle(player)) then
			local a,b,c = getElementVelocity(getPedOccupiedVehicle(player))
			local x,y,z = getElementPosition(getPedOccupiedVehicle(player))
			if ((((a^2 + b^2 + c^2)^(0.5))*180) < 20) then
				local d,e,f = getElementRotation(getPedOccupiedVehicle(player))
				local yoffset = math.sin(math.rad(f+90))*10
				local xoffset = math.cos(math.rad(f+90))*10
				local zoffset = math.sin(math.rad(d))*10
				mine[player] = createObject(2918,x+xoffset,y+yoffset,z+zoffset)
			elseif ((((a^2 + b^2 + c^2)^(0.5))*180) < 160) then
				mine[player] = createObject(2918,x+a*50,y+b*50,z+c*50)
			else 
				mine[player] = createObject(2918,x+a*10,y+b*10,z+c*10)
			end
	    end
	end
end

a = -2230
b = 1819
for i=-2480,-1980,3 do
y = b - math.sqrt((250^2)-(i-a)^2)
createObject(3763,i,y,0) 
y = b + math.sqrt((250^2)-(i-a)^2)
createObject(3763,i,y,0) 
end
