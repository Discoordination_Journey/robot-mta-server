--setDevelopmentMode(true)

-- addEventHandler ( "onClientElementDataChange", getRootElement(),
-- function ( dataName )
	-- if dataName == "hl.racestate" then
		-- local state = getElementData (source, "hl.racestate"))
		
		-- if state == "LoadingMap" then
			
		-- end
		
		-- if state == "GridCountdown" then
			
		-- end
	-- end
-- end )
	
function collide()
	localVeh = getPedOccupiedVehicle(localPlayer)
	for i,p in ipairs (getElementsByType("player")) do
		local veh = getPedOccupiedVehicle(p)
		if veh and localVeh then
			setElementCollidableWith(localVeh,veh,false)
		end
	end
end
addEventHandler("onClientRender",root,collide)
	
function teleport(hitElement)
	if (getElementType(hitElement) == "vehicle") then
		if (getVehicleOccupant(hitElement) == localPlayer) then
			setTimer(setTime,3000,1,23,0)
		end
	end
end
time1 = createColCircle(2581.7,-2125,5)
addEventHandler("onClientColShapeHit",time1,teleport)


explosion1 = createColCircle(1894.2,-1828,10)
function createExplosion1(hitElement)
	if getElementType(hitElement) =="vehicle" then
		if (getVehicleOccupant(hitElement) == localPlayer) then
			createExplosion(1952.4,-1844.5,3.9,10)
		end
	end
end
addEventHandler("onClientColShapeHit",explosion1,createExplosion1)

explosion2 = createColCircle(2452.3,-1846.3,10)
function createExplosion2(hitElement)
	if getElementType(hitElement) =="vehicle" then
		if (getVehicleOccupant(hitElement) == localPlayer) then
			createExplosion(2510.1,-1847.9,14.7,10)
		end
	end
end
addEventHandler("onClientColShapeHit",explosion2,createExplosion2)



function enable()
	enableGun()
end
addCommandHandler("gun",enable)
	
for index,thePed in ipairs (getElementsByType("ped")) do
	local model = getElementModel(thePed)
	if (model == 22) or (model == 23) or (model == 24) or (model == 25)then
		setPedControlState(thePed,"fire",true)
		addEventHandler( "onClientElementStreamIn", getRootElement( ), --regive the weapon serverside to stop it from glitching. If this is not done the peds will only fire one bullet and then stop
		function ( )
			triggerServerEvent ( "regiveweapon", localPlayer, thePed)
			setPedControlState(thePed,"fire",true)
		end
		);
	end
end

function walkingPeds()
	for index,thePed in ipairs (getElementsByType("ped")) do
		if getElementModel(thePed) ==29 then
			setPedControlState(thePed,"forwards",true)
			
			if math.random()<0.8 then
				setPedControlState(thePed,"walk",true)
			end
			
			if math.random()<0.3 then
				setPedControlState(thePed,"crouch",true)
			end
			setTimer(rotateWalkingPed,2000,0,thePed)
		end
	end
end

function rotateWalkingPed(thePed) --make the zombie peds change direction every 2000 seconds.
	if math.random() < 0.5 then
		setPedControlState(thePed,"left",true)
		setPedControlState(thePed,"right",false)
	else
		setPedControlState(thePed,"left",false)
		setPedControlState(thePed,"right",true)
	end
end
walkingPeds()

function pedTarget() --rotate the peds to face towards the player at all times
	for index,thePed in ipairs (getElementsByType("ped")) do
		local ex, ey, ez = getElementPosition(thePed)           -- 3D point that needs to be rotated
		local px, py, pz = getElementPosition(localPlayer)  -- 3D point that needs to be rotated to
		 
		local rotx = math.atan2 (pz - ez, getDistanceBetweenPoints2D ( px, py, ex, ey ) )
		local roty = 0
		local rotz = math.pi - math.atan2 ( ( ex - px ), ( ey - py ) )
		 
		rotx, roty, rotz = math.deg (rotx), math.deg(roty), math.deg(rotz)
		if getElementModel(thePed) ~= 29 then --29 are zombies peds and are excluded
			setElementRotation(thePed, rotx, roty, rotz)
			setPedAimTarget(thePed,px,py,pz)
		end
	end
end
addEventHandler("onClientRender",root,pedTarget)


