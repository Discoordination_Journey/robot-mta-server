addEvent( "createTrailer", true )

thisroot = getRootElement()

addEventHandler("createTrailer", root, function(rotX, rotY, rotZ, x, y, z, vehicle, player) 
	
	local trailer = createVehicle ( 435, x, y, z, rotX, rotY, rotZ )    -- create a trailer
	setElementCollisionsEnabled(trailer, false)
	attachTrailerToVehicle(vehicle, trailer)
	--outputChatBox ("createVehicle")

	triggerClientEvent( player, "getTrailerCreated", root, trailer)
end 
)

addEvent( "attachTrailer", true )
addEventHandler("attachTrailer", root, function(rotX, rotY, rotZ, x, y, z, vehicle, trailer, player) 
	setElementPosition(trailer, x, y, z)
	setElementRotation(trailer, rotX, rotY, rotZ)
	--outputChatBox ("attachTrailer")
	
	attachTrailerToVehicle(vehicle, trailer)
	triggerClientEvent( player, "getTrailerCreated", root, trailer)
end 
)