local trailer
local vehicle
local drawText = false
local screenWidth, screenHeight = guiGetScreenSize ( )
local firstSpawn = true

local localPlayer = getLocalPlayer()

function getPositionFromElementOffset(element,offX,offY,offZ)
	local m = getElementMatrix ( element )  -- Get the matrix
	local x = offX * m[1][1] + offY * m[2][1] + offZ * m[3][1] + m[4][1]  -- Apply transform
	local y = offX * m[1][2] + offY * m[2][2] + offZ * m[3][2] + m[4][2]
	local z = offX * m[1][3] + offY * m[2][3] + offZ * m[3][3] + m[4][3]
	return x, y, z                               -- Return the transformed point
end

addEventHandler("onClientPlayerSpawn", getLocalPlayer(), function() 
	if firstSpawn then
		setTimer( function ()
			vehicle = getPedOccupiedVehicle(localPlayer)
		
			local rotX, rotY, rotZ = getElementRotation(vehicle)
			
			local x,y,z = getPositionFromElementOffset(vehicle,0,-10,0)
			
			triggerServerEvent( "createTrailer", localPlayer, rotX, rotY, rotZ, x, y, z, vehicle, localPlayer)
			firstSpawn = false
			--outputChatBox ("createTrailer")
		end, 100, 1)
	else
		setTimer(attachTrailer, 200, 1)
		--outputChatBox ("setTimer")
	end
end 
)



function attachTrailer()
	vehicle = getPedOccupiedVehicle(localPlayer)
	
	local rotX, rotY, rotZ = getElementRotation(vehicle)
	
	local x,y,z = getPositionFromElementOffset(vehicle,0,-10,0)
	
	triggerServerEvent( "attachTrailer", localPlayer, rotX, rotY, rotZ, x, y, z, vehicle, trailer, localPlayer)
	
	--triggerServerEvent ( "onGreeting", localPlayer, "Hello World!" ) 
	
	--trailer = createVehicle ( 435, x, y, z, rotX, rotY, rotZ )    -- create a trailer
	
	--outputChatBox ("attachTrailer")
end

addEvent( "getTrailerCreated", true )
addEventHandler ( "getTrailerCreated", getRootElement(), function(serverTrailer)
	trailer = serverTrailer
	setTimer(checkAttached, 1000, 1)
end
)

function checkAttached()
	vehX, vehY, vehZ = getElementPosition(vehicle)
	traX, traY, traZ = getElementPosition(trailer)
	
	distance = getDistanceBetweenPoints3D(vehX, vehY, vehZ, traX, traY, traZ)
	
	--outputChatBox(distance)

	if (distance > 15) then
		setElementHealth(localPlayer, 0)
		--outputChatBox ("killPlayer")
		dxDrawText ( "You must keep your trailer attached!", 44, 41)
		drawText = true
		setTimer(stopDrawing, 5000, 1)
	else 
		setTimer(checkAttached, 500, 1)
	end
end

function stopDrawing()
	drawText = false
end

function drawing()	
	if drawText then
		dxDrawText ( "Keep your trailer attached!", 250, 250, 1000, 1000, tocolor ( 255, 50, 50, 255 ), 2, "pricedown")
	end
end

function HandleTheRendering ( )
    addEventHandler ( "onClientRender", root, drawing ) -- keep the text visible with onClientRender.
end
 
addEventHandler ( "onClientResourceStart", resourceRoot, HandleTheRendering )

	
