function createTrailer(x, y, z, rotX, rotY, rotZ)
	local vehicle = getPedOccupiedVehicle(client)

	local trailer = createVehicle ( 435, x, y, z, rotX, rotY, rotZ )   -- create a trailer
	attachTrailerToVehicle ( vehicle, trailer )   -- attach trailer to truck
	setElementAlpha(trailer, 155)	-- make trailer semi-transparent
	
	triggerClientEvent(client, "sendClientTrailer", client, trailer)
end
addEvent( "createTrailer", true )
addEventHandler( "createTrailer", resourceRoot, createTrailer )