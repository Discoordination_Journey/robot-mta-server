DATABASE = dbConnect("sqlite", ":/achievements.db")

function calculatePercentage(group, achievement)
	-- Find maximum players that ever got first achievement
	dbExec(DATABASE, "CREATE TABLE IF NOT EXISTS leaderboards(accountname TEXT, playername TEXT, count INTEGER)")
	local outputResults = dbPoll(dbQuery(DATABASE, "SELECT COUNT(*) as playerscount FROM leaderboards"), -1)
	local maximumPlayers = tonumber(outputResults[1]["playerscount"] or 0)
	local achievementPlayers = 0
	
	-- Find how many players got this achievement
	local tableName = group.. "" ..achievement
	
	if achievementsList[group][achievement][3] == 1 then
		outputResults = dbPoll(dbQuery(DATABASE, "SELECT COUNT(*) as playerscount FROM " ..tableName), -1)
		achievementPlayers = tonumber(outputResults[1]["playerscount"] or 0)
	else
		outputResults = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..tableName), -1)
		if outputResults and #outputResults > 0 then
			for i = 1, #outputResults do
				if achievementsList[group][achievement][3] == outputResults[i]["stage"] then achievementPlayers = achievementPlayers + 1 end
			end
		end
	end
	
	return achievementPlayers / (maximumPlayers / 100);
end

function triggerAchievement(player, group, achievement, data)
	if not DATABASE then return end
	if player == nil or not player then return end
	if getElementType(player) ~= "player" then return end
	if isGuestAccount(getPlayerAccount(player)) then return end
	
	if data == nil then data = {} end
	
	-- Create table in DATABASE
	local tableName = group.. "" ..achievement
	dbExec(DATABASE, "CREATE TABLE IF NOT EXISTS " ..tableName.. "(accountname TEXT, timestamp INTEGER, stage INTEGER, data TEXT)")
	
	-- Check if player already had this achievement
	local results = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..tableName.. " WHERE accountname = ?", getAccountName(getPlayerAccount(player))), -1)
	if results and #results > 0 then
		-- Player already had this achievement or its ongoing
		if achievementsList[group][achievement][3] > 1 then
			-- The achievement has more than 1 stages
			
			if results[1]["stage"] + 1 < achievementsList[group][achievement][3] then
				-- Update the stage counter
				dbExec(DATABASE, "UPDATE " ..tableName.. " SET stage = ?, data = ? WHERE accountname = ?", results[1]["stage"] + 1, toJSON(data), getAccountName(getPlayerAccount(player)))
			elseif results[1]["stage"] + 1 == achievementsList[group][achievement][3] then
				-- Show achivement popup
				dbExec(DATABASE, "UPDATE " ..tableName.. " SET stage = ?, data = ?, timestamp = ? WHERE accountname = ?", results[1]["stage"] + 1, toJSON(data), os.time(), getAccountName(getPlayerAccount(player)))
				triggerClientEvent(player, "showPopup", player, group, achievement, calculatePercentage(group, achievement), data)
				outputChatBox("#00FF00[Achievements] " ..getPlayerName(player).. "#00FF00 got an achievement: '#E7D9B0" ..achievementsList[group][achievement][2].. "#00FF00'.", root, 0, 0, 0, true)
				
				-- Update leaderboard
				dbExec(DATABASE, "CREATE TABLE IF NOT EXISTS leaderboards(accountname TEXT, playername TEXT, count INTEGER)")
				local leaderboardResuls = dbPoll(dbQuery(DATABASE, "SELECT * FROM leaderboards WHERE accountname = ?", getAccountName(getPlayerAccount(player))), -1)
				if leaderboardResuls and #leaderboardResuls > 0 then
					-- Player alredy had record in leaderboards
					dbExec(DATABASE, "UPDATE leaderboards SET count = ?, playername = ? WHERE accountname = ?", leaderboardResuls[1]["count"] + 1, getPlayerName(player), getAccountName(getPlayerAccount(player)))
				else
					-- First time in leaderboards
					dbExec(DATABASE, "INSERT INTO leaderboards(accountname, playername, count) VALUES (?,?,?)", getAccountName(getPlayerAccount(player)), getPlayerName(player), 1)
				end
			end
		end
	else
		if achievementsList[group][achievement][3] > 1 then
			-- The achievement has more than 1 stages
			dbExec(DATABASE, "INSERT INTO " ..tableName.. "(accountname, timestamp, stage, data) VALUES (?,?,?,?)", getAccountName(getPlayerAccount(player)), 0, 1, toJSON(data))
		else
			-- Achievement with only one stage
			dbExec(DATABASE, "INSERT INTO " ..tableName.. "(accountname, timestamp, stage, data) VALUES (?,?,?,?)", getAccountName(getPlayerAccount(player)), os.time(), 1, toJSON(data))
			triggerClientEvent(player, "showPopup", player, group, achievement, calculatePercentage(group, achievement), data)
			outputChatBox("#00FF00[Achievements] " ..getPlayerName(player).. "#00FF00 got an achievement: '#E7D9B0" ..achievementsList[group][achievement][2].. "#00FF00'.", root, 0, 0, 0, true)
			
			-- Update leaderboard
			dbExec(DATABASE, "CREATE TABLE IF NOT EXISTS leaderboards(accountname TEXT, playername TEXT, count INTEGER)")
			local leaderboardResuls = dbPoll(dbQuery(DATABASE, "SELECT * FROM leaderboards WHERE accountname = ?", getAccountName(getPlayerAccount(player))), -1)
			if leaderboardResuls and #leaderboardResuls > 0 then
				-- Player alredy had record in leaderboards
				dbExec(DATABASE, "UPDATE leaderboards SET count = ?, playername = ? WHERE accountname = ?", leaderboardResuls[1]["count"] + 1, getPlayerName(player), getAccountName(getPlayerAccount(player)))
			else
				-- First time in leaderboards
				dbExec(DATABASE, "INSERT INTO leaderboards(accountname, playername, count) VALUES (?,?,?)", getAccountName(getPlayerAccount(player)), getPlayerName(player), 1)
			end
		end
	end	
end


addEvent("getAchievements", true)
addEventHandler("getAchievements", getRootElement(), function()
	-- Get achievements for player
	local data = {}
	for k, v in pairs(achievementsList) do
		data[k] = {}
		for i = 1, #achievementsList[k] do
			-- Table management
			local tableName = k.. "" ..i
			dbExec(DATABASE, "CREATE TABLE IF NOT EXISTS " ..tableName.. "(accountname TEXT, timestamp INTEGER, stage INTEGER, data TEXT)")
			
			-- Check data
			local results = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..tableName.. " WHERE accountname = ?", getAccountName(getPlayerAccount(source))), -1)
			
			if results and #results > 0 then
				if achievementsList[k][i][3] > 1 and results[1]["stage"] >= achievementsList[k][i][3] then
					-- Achievement had more then 1 stage and player got it
					data[k][i] = {true, results[1]["stage"], calculatePercentage(k, i), results[1]["timestamp"]}
				elseif achievementsList[k][i][3] > 1 and results[1]["stage"] < achievementsList[k][i][3] then
					-- Achievement had more then 1 stage and player still in progress
					data[k][i] = {false, results[1]["stage"], calculatePercentage(k, i)}
				else
					-- Achievement had 1 stage and player got it
					data[k][i] = {true, 1, calculatePercentage(k, i), results[1]["timestamp"]}
				end
			else
				-- Player never done this achievement
				data[k][i] = {false, 0, calculatePercentage(k, i)}
			end
		end
	end
	
	-- Get leaderboards
	dbExec(DATABASE, "CREATE TABLE IF NOT EXISTS leaderboards(accountname TEXT, playername TEXT, count INTEGER)")
	local leaderboard = dbPoll(dbQuery(DATABASE, "SELECT * FROM leaderboards ORDER BY count DESC LIMIT 40"), -1)
	
	triggerClientEvent(source, "getAchievementsFromServer", source, data, leaderboard)
end )

function getAchievementData(player, group, achievement)
	if not DATABASE then return end
	if isGuestAccount(getPlayerAccount(player)) then return end
	
	-- Table management
	local tableName = group.. "" ..achievement
	dbExec(DATABASE, "CREATE TABLE IF NOT EXISTS " ..tableName.. "(accountname TEXT, timestamp INTEGER, stage INTEGER, data TEXT)")
	
	-- Check data
	local results = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..tableName.. " WHERE accountname = ?", getAccountName(getPlayerAccount(player))), -1)
	if results and #results > 0 then return results[1]["data"]
	else return false end
end