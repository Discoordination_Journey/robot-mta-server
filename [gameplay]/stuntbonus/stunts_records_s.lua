local DATABASE = nil

local stuntsNames = {
	"Maximum Insane Jump Distance",
	"Maximum Insane Jump Height",
	"Maximum Insane Jump Flips",
	"Maximum Insane Jump Rotation",
	"Longest Stoppie Distance",
	"Longest Stoppie Time",
	"Longest Wheelie Distance",
	"Longest Wheelie Time", 
	"Longest 2 Wheels Distance",
	"Longest 2 Wheels Time"
};

local stuntTables = {
	"InsaneDistance",
	"InsaneHeight",
	"InsaneFlips",
	"InsaneRotation",
	"StoppieDistance",
	"StoppieTime",
	"WheelieDistance",
	"WheelieTime",
	"WheelsDistance",
	"WheelsTime"
};

addEventHandler("onResourceStart", resourceRoot, function()
	-- Load database
	DATABASE = dbConnect("sqlite", "stuntsRecords.db")
	if not DATABASE then return end
	
	for i = 1, #stuntTables do
		-- Creating the  tables
		dbExec(DATABASE, "CREATE TABLE IF NOT EXISTS " ..stuntTables[i].. "(playername TEXT, score DOUBLE)")
	end
end )

addEvent("checkStunt", true)
addEventHandler("checkStunt", getRootElement(), function(stuntType, stuntData)
	if not DATABASE then return end 
	--outputChatBox(tostring(toJSON(stuntData))) -- DEBUG
	
	-- Insane Stunt Bonus
	if stuntType == 1 then
		if stuntData[1] > 20000 or stuntData[2] > 20000 then return end
		for i = 1, 4 do
			if stuntData[i] == 0 then return end
			local recordUpdated = false
			
			-- Select Data
			recordsResults = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..stuntTables[i].. " WHERE playername = ?", getPlayerName(source)), -1)		

			-- Update Data
			if (recordsResults and #recordsResults > 0) then
				if recordsResults[1]["score"] < stuntData[i] then 
					dbExec(DATABASE, "UPDATE " ..stuntTables[i].. " SET score = ? WHERE playername = ?", stuntData[i], getPlayerName(source))
					recordUpdated = true
				end
			else
				dbExec(DATABASE, "INSERT INTO " ..stuntTables[i].. "(playername, score) VALUES (?,?)", getPlayerName(source), stuntData[i])
				recordUpdated = true
			end
			
			-- WR check
			checkResults = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..stuntTables[i].. " ORDER BY score DESC LIMIT 1"), -1)
			if checkResults[1]["playername"] == getPlayerName(source) and recordUpdated then
				if i < 3 then
					outputChatBox("#00FF00New " ..stuntsNames[i].. ": " ..getPlayerName(source).. "#00FF00, " ..string.format("%.1f", stuntData[i]).. " meters", root, 255, 255, 255, true)
				elseif i == 3 then
					outputChatBox("#00FF00New " ..stuntsNames[i].. ": " ..getPlayerName(source).. "#00FF00, " ..string.format("%.1f", stuntData[i]).. " flips", root, 255, 255, 255, true)
				elseif i == 4 then
					outputChatBox("#00FF00New " ..stuntsNames[i].. ": " ..getPlayerName(source).. "#00FF00, " ..string.format("%.1f", stuntData[i]).. " degrees", root, 255, 255, 255, true)
				end
			end	
		end
	elseif stuntType == 2 or stuntType == 3 or stuntType == 4 then
		-- 2 - Stoppie
		-- 3 - Wheelie
		-- 4 - 2 wheels
		
		local recordUpdated = false
		for i = 1, 2 do
			if stuntData[i] == 0 then return end
			local recordUpdated = false
			
			-- Select Data
			recordsResults = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..stuntTables[stuntType*2 + i].. " WHERE playername = ?", getPlayerName(source)), -1)		

			-- Update Data
			if (recordsResults and #recordsResults > 0) then
				if recordsResults[1]["score"] < stuntData[i] then 
					dbExec(DATABASE, "UPDATE " ..stuntTables[stuntType*2 + i].. " SET score = ? WHERE playername = ?", stuntData[i], getPlayerName(source))
					recordUpdated = true
				end
			else
				dbExec(DATABASE, "INSERT INTO " ..stuntTables[stuntType*2 + i].. "(playername, score) VALUES (?,?)", getPlayerName(source), stuntData[i])
				recordUpdated = true
			end
			
			-- WR check
			checkResults = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..stuntTables[stuntType*2 + i].. " ORDER BY score DESC LIMIT 1"), -1)
			if checkResults[1]["playername"] == getPlayerName(source) and recordUpdated then
				if i == 1 then outputChatBox("#00FF00New " ..stuntsNames[stuntType*2 + i].. ": " ..getPlayerName(source).. "#00FF00, " ..string.format("%.1f", stuntData[i]).. " meters", root, 255, 255, 255, true)
				elseif i == 2 then outputChatBox("#00FF00New " ..stuntsNames[stuntType*2 + i].. ": " ..getPlayerName(source).. "#00FF00, " ..string.format("%.1f", stuntData[i]).. " seconds", root, 255, 255, 255, true) end
			end	
		end
	end
end )

addEvent("getStuntsStats", true)
addEventHandler("getStuntsStats", getRootElement(), function()
	if DATABASE then
		-- Get data: WR, then PB
		local data = {}
		for i = 1, #stuntTables do
			data[i] = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..stuntTables[i].. " ORDER BY score DESC LIMIT 1"), -1)
			data[#stuntTables+i] = dbPoll(dbQuery(DATABASE, "SELECT * FROM " ..stuntTables[i].. " WHERE playername = ?", getPlayerName(source)), -1)
		end
		
		triggerClientEvent(source, "receiveStuntsStats", source, data)
	end
end )