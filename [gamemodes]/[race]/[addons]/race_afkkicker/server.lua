local RACE_STATE = ""

function afkCheck()
	if
		RACE_STATE == "Running"
		or RACE_STATE == "MidMapVote"
		or RACE_STATE == "SomeoneWon"
	then
		local AFK_TRIGGER_TIME = tonumber(get("afk_timer")) or 30

		for _, player in ipairs(getElementsByType("player")) do
			local idleTime = getPlayerIdleTime(player) / 1000

			if
				not isPedDead(player)
				and not getElementData(player, "race.spectating")
				and isPedInVehicle(player)
				and not isElementFrozen(getPedOccupiedVehicle(player))
			then
				if idleTime >= AFK_TRIGGER_TIME - 5 then
					triggerClientEvent(player, "warnAFK", player)
				end

				if idleTime >= AFK_TRIGGER_TIME then
					triggerClientEvent(player, "goAFK", player)
				end

				if idleTime >= AFK_TRIGGER_TIME + 5 then
					triggerEvent("onRequestKillPlayer", player)
				end
			end
		end
	end
end
setTimer(afkCheck, 1000, 0)

function onRaceStateChanging(newStateName)
	RACE_STATE = newStateName
end
addEvent("onRaceStateChanging", true)
addEventHandler("onRaceStateChanging", root, onRaceStateChanging)