local SHOW_AFK_WARNING = false

function goAFK()
	executeCommandHandler("spectate")
end
addEvent("goAFK", true)
addEventHandler("goAFK", root, goAFK)

function warnAFK()
	SHOW_AFK_WARNING = true

	setTimer(function()
		SHOW_AFK_WARNING = false
	end, 500, 1)
end
addEvent("warnAFK", true)
addEventHandler("warnAFK", root, warnAFK)

function onPreRender()
	local screenWidth, screenHeight = guiGetScreenSize()

	if SHOW_AFK_WARNING then
		dxDrawBorderedText(
			1,
			"AFK!",
			(screenWidth / 2),
			(screenHeight / 2),
			(screenWidth / 2),
			(screenHeight / 2),
			tocolor(255, 255, 255, 255),
			10,
			"pricedown",
			"center",
			"center"
		)
	end
end
addEventHandler("onClientPreRender", root, onPreRender)