# ROBOT MTA SERVER

## Contributing

### General

To contribute, make a pull request for the **master** branch. After review, your contribution will be added.

The server currently uses version 1.6.0-9.22396.0

##### Overview of folders

If you would like to contribute you will most likely work with these resources:

* **[editor]** -> The map editor, with custom fixes applied. You may use your own instead, but we recommend using this one if you can.
*  **[gamemodes]/[race]/race** -> Race resource.
* **[gamemodes]/[race]/[addons]** -> Various addons to the race resource, such as the progress bar, ghosts, and toptimes.
* **[gameplay]** -> Gameplay add-on resources such as extra air resistance, pole remover, hot potatoes, etc.
*  **[managers]** -> Add-on resources such as MOTD, nickprotection, etc.
*  **[templates]** -> Commonly used scripts, if you need something copy it from here.
*  **[maps]** -> This is the good stuff, all maps on the server.
----------------------
### Maps
#### Basic requirements
In order for the server to accept your map at all, your map's folder must be at minimum placed in the **correct subfolder**, have its **racetype tag** set in the meta.xml, and be **named correctly**. Remember to manually check that everything in the meta.xml file is correct before sending the map, using the following instructions. Note that the map editor has a tendency to randomly erase data from the meta.xml, so be vigilant for missing data.

If this is all too complicated or scares you, leave a note with your submission and we can do it for you.

##### Subfolder
Place your map in the **[maps]** folder, inside the relevant subfolders:
* **[R]** -> Race: Ordinary checkpoint-to-checkpoint races.
* **[SR]** -> Speedrun: Race based on a singleplayer mission or speedrun route. Good for practising, finding new routes, or enjoying the singleplayer campaign with your friends or strangers.
* **[DD]** -> Destruction Derby: Take out other players and attempt to be the last one standing!
* **[TW]** -> Teamwork: Work together with a randomly selected team in order to try and be the first team to complete a course. Some of your team will be required to help your other teammates complete the course.
* **[MG]** -> Minigame: Beat other players in a map-specific task to win, which is explained to you when you play them.
* **[F]** -> Footrace: Races in which you can exit your vehicle and collect checkpoints on foot.

These folders may be further subdivided as well, but these subdivisions should be self-explanatory.

##### Racetype tag
Maps require information inside the meta.xml so the server can get some custom information about them. (read section *Directory layout for MTA:SA resources* for why just placing them in the correct folder won't suffice.)

Formerly, map tags (*R/F/DD/TW/MG/SR*) were to be included directly in the map name. Please now include these as a separate attribute instead, like so:
```xml
<info gamemodes="race" type="map" name="Name" author="You" version="1.0" racetype="DD"></info>
```

Alternatively, you may include them as a setting instead, like so:
```xml
<setting name="#racetype" value="DD"></setting>
```

Doing this will help the server prepend the tags in front of the options in the map vote UI like so: ``[DD] A Destruction Derby``. Failure to do this will cause maps to show up with the tag ``[???]``.

##### Folder name
Each map has a folder name, which may or may not match the map's name defined in the meta.xml. To prevent folder chaos, please ensure that it does. Look at the other maps and match the format. For more details read this:
* The map folder should be in PascalCase.
* Only use the letters A-Z, a-z, and digits 0-9. Remove punctuation and accent marks. Use the letter Q to represent question-marks.
* Make it match the actual map name.
* Dashes may be used to separate series (eg. Infernus Showdown/NC202154) or map numbers (for classic maps from the 2005-2008 archive whose original names have been lost) from the map title.

----------------------
#### Guidelines, tips, strong recommendations.
##### Settings (meta.xml)
###### Mandatory
These settings are mandatory and you should probably set them.
* ``#ghostmode`` on **true** for typical races/speedruns, **false** for typical destruction derbies. Exceptions exist, but 95% of maps follow this standard.
* ``#respawntime`` to **1** or **-1** (negative one) to use the server default of 1 second. Use **0** for extremely difficult maps. Higher values possible with good justifications.
* ``#useLODs`` to **true**. Only set this to false if map performance really tanks a lot. This setting does not matter for maps that use no customly placed objects. For more information, see section *mapEditorScriptingExtension*.

Example of these settings at their default values:
```xml
<setting name="#ghostmode" value="[ true ]"></setting>
<setting name="#respawntime" value="[ -1 ]"></setting>
<setting name="#useLODs" value="[ true ]"></setting>
```
###### Custom
We have various custom settings on this server.
* ``#timeafterfirstfinish`` -> By default, the server will set the overtime to 50% of the number 1 finisher's time. Use this setting to override this behavior and set the overtime to a fixed integer (in seconds) instead. This setting is recommended for maps that can have wildly varying finish times with a very short top time, or difficult maps where the top time is more than twice as fast as the average. Set it to -1 to use the server default of 50%. Examples of maps using this are *Olympic Jump*, *Coin Toss Journey*, *Going Up 1*, and *Grand Rally Randomizer (254 CP KEKW)*.
* ``#countdownduration`` -> To adjust the grid countdown duration for your map. Set it to an integer (in seconds). The default countdown duration is 3. Change it for maps that play out a cutscene or other scripted sequence during the countdown sequence. Setting it to -1 will use the default server setting of 3. Note that when you set it to values higher than 3, those numbers will be invisible and will not be accompanied with a flashing countdown timer and beep sound. Examples of maps using this are *Airpain 3*, *Disco Drive*, *The Heist*, and *O Luna, Illuminate Us*.
* ``#disableraceghosts`` -> To disable race ghosts for your map, set this to true. This is recommended for overly long maps without a distinct route, maps where ghosts affect the gameplay or spoil a puzzle solution, or just maps where they add no value. Examples of maps using this are *Super FleischBerg© Autos*, *Paramedic - San Andreas*, *Pole Slut Heaven*, *Find Your Way*, and *Most Wanted: Los Santos*.
* ``#shufflespawns`` -> Setting this to true will make the map choose spawnpoints at random. By default, it will pigeon hole players into a predetermined order of spawnpoints. DD maps might benefit from this in particular. Examples of maps using this are *Disco Derby 3D*, and *Filler Map Quick Latency Test*.

Example of these settings at their default values:
```xml
<setting name="#timeafterfirstfinish" value="[ -1 ]"></setting>
<setting name="#countdownduration" value="[ -1 ]"></setting>
<setting name="#disableraceghosts" value="[ false ]"></setting>
<setting name="#shufflespawns" value="[ false ]"></setting>
```

##### Map design
###### Spawnpoints
* 1 spawnpoint for non-collision races. More spawnpoints can be used to reduce visual clutter at the start, but ensure the spawnpoints are fair (ie: No long Formula 1'esque grid formations stretching for miles)
* 128 spawnpoints for collision races/derbies/etc. Lower numbers will cause the map to fail to load when many players are present.
* Place spawnpoints slightly above the ground so they do not produce tyre smoke during the countdown which helps against FPS drops.

###### Checkpoints
This server supports the following custom attributes for race checkpoints. Note that these are not backwards compatible with older or different versions of the race gamemode as found on other servers. You will also need to download this repository&apos;s race resource if you want to try these out for yourself:
* ``blipsize`` -> Supporting integer values 0-25, use this to adjust the blip's size of the checkpoint. By default this was and is set to 2 for the current checkpoint and 1 for the next checkpoint. This is recommended for maps where the checkpoints are far away and bigger blips are desired so it's easier to find them on the maximap (F11). Alternatively, this is useful for checkpoints that are activated by script instead of hit normally; by setting the blipsize to 0 these checkpoints will be completely invisible instead of manifesting themselves as a blip placed far away, distracting or confusing players. Examples of maps using this are *Pack Mule*, *Import/Export Race*, and *Find Your Way*.
* ``extracolshapesize`` -> Supporting integer values, use this to increase or decrease a checkpoint's hitbox size. By default this was and is set to 4, meaning each checkpoint, regardless of size, had an extra 4 meters or so added to its hitbox radius. With this setting, you can customize this number to make a checkpoint&apos;s hitbox much bigger, or smaller, than its visual appearance. Setting it to 0 makes the hitbox equal to the visible marker. Negative values are possible to make the hitbox smaller, with the lowest possible value being negative the checkpoint&apos;s visible size. Please use this SPARINGLY. There will be no visible indicators that a particular checkpoint&apos;s size is different so making unobvious changes can lead to confusion or frustration. It is recommended to continue placing barriers on the sides of checkpoints, or creating a custom visual indicator. To test: Update your race gamemode resource to ours, and use the devmode resource to visualize the hitboxes. For an example of a map using this, see *Dutch Mountain Biking*, showcasing a map with decreased checkpoint hitboxes.

This server has a custom ``setCheckpointText`` exported function which you can use to set the checkpoint text in the bottom-right to what it should be according to the map. Note that currently the checkpoint count is wrong when spectating and is wrong on the scoreboard and that attempting to call this function on old race gamemodes will throw an error, but it should not break backwards compatibility. Examples of maps using this are *Super FleischBerg© Autos*, *Procedurally Generated Race*, and *Player for Generated Races*.

##### Map scripting
You may want your maps to have some scripting in them. See the **[templates]** folder for useful scripts. Here are some notes:

###### Music
Our server focuses a lot on streamers. To prevent them from getting copystruck, when adding custom music to a map make sure it is set to OFF by default. You can use the *music_c.lua* template to do this for you. Simply edit the few lines at the top of the file to match your music file.

###### Extra Resources
This server has some resources you can include in your map by adding them to your map's meta.xml file like so:
```xml
<include resource="ResourceName"></include>
```
* ``poleremover`` -> Easily remove all of the lampposts and such from the world without you doing it yourself.
* ``extraairresistance`` -> Easily disable extra air resistance from roads without you making or finding a script to do it yourself. Here's a map of where it's normally applied for reference - https://ehgames.com/gta/map/vfk7w5kit79.
* ``randomfoliage`` -> Easily remove all of the randomly generated foliage (small trees, bushes, maybe rocks) from the world without you making or finding a script to do it yourself.
* ``bomb`` -> Give everyone a Sandking and give a random player a bomb every 30 seconds. The player who is holding the bomb needs to ram someone to pass it to them and whoever is holding the bomb at the end of the 30 second countdown is eliminated, like Hot Potato. Examples of maps using this are *Bushland* and *Fallout*.

###### mapEditorScriptingExtension
Unlike what their name suggests, these files also do things outside the map editor. They deal with: 
1. Long distance visibility of customly placed objects.
2. Removal of world objects, ensuring it works correctly.
3. Breakability of certain customly placed objects, ensuring it works correctly.

If you have neither
* Placed extra objects in the map, particularly large ones such as buildings, roads, or terrain;
* Used the 'remove world object' tool in the map editor; or
* Placed breakable objects, such as hay bales or explosive barrels

then these files are not needed and you can delete them to help us save disk space :) . Don't forget to also remove the script nodes for them in the meta.xml file.

We use a custom script to improve the first point listed above. We strongly recommend you use the map editor shipped with this server, found in the **[editor]** folder. It will automatically save maps with our custom *mapEditorScriptingExtension* (MESE) file.

Otherwise, if you are using your own editor, and have placed extra objects in the map, particularly large ones such as buildings, roads, or terrain, please follow these steps:

1. If your file is named ``customMapEditorScriptingExtension_s`` (with the *custom* prefix) and is less than 91 kb in size (more likely in the 3-10 kb range), you are using our editor and no action is required.
2. If your file is named ``newMapEditorScriptingExtension_s`` (with the *new* prefix) and is less than 70 kb in size (more likely in the 3-30 kb range), you are most likely using an older version of our editor and no action is required. If any LODs show up incorrectly, please take the template ``mapEditorScriptingExtension_s`` from the **[Templates]** folder and use that instead.
3. If your file is named ``newMapEditorScriptingExtension_s`` (with the *new* prefix) and is 70 kb in size, you are using an experimental map editor version by the official MTA team. If you haven't changed the ``scale`` or ``alpha`` properties of any objects no action is required. Otherwise, take the template ``mapEditorScriptingExtension_s`` from the **[Templates]** folder and use that instead.
4. If your file is named ``mapEditorScriptingExtension_s`` (without a prefix) and is less than 91 kb in size, please take the template ``mapEditorScriptingExtension_s`` from the **[Templates]** folder and use that instead.
5. If your file is named ``mapEditorScriptingExtension_s`` (without a prefix) and is 91 kb in size, you already followed these steps. No action is required.

We do not care about ``MapEditorScriptingExtension_c`` (ending in *c* instead of *s*). Do with it what you want.

## Technical mumbo jumbo
#### Directory layout for MTA:SA resources

Important things to note:

* Any directory surrounded with [...] will be assumed to contain further directories and resources.
* The directory layout is to assist organising resources in the server directory only. Internally MTA:SA still sees all the resources in a big flat list.
* Therefore, do not use the [...] paths in script or config files.
* Therefore, you can move resources around without having to worry about [...] paths.
* But, a resource will not load if it exists twice anywhere in the directory hierarchy.
* And finally, this is only a suggested layout. You can move stuff around, although it's probably best to leave the official resources where they are for simpler updating.
