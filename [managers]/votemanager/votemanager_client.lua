local voteWindow
local boundVoteKeys = {}
local nameFromVoteID = {}
local voteIDFromName = {}

local progressbars = {}

local screenX,screenY = guiGetScreenSize()

local pollBoxRectDefault = {
	x = screenX - (screenX/5) - 10,
	y = screenY - (screenY/(screenY/432)) - 10,
	width = screenX/5,
	height = screenY/(screenY/432)
	}
local pollBoxRect = pollBoxRectDefault

local pollTitle = "Vote for the next map"
local pollOptionsText = {}

local pollVotes = {}
local pollMaxVoters = 0
local pollPassPercentage = 2
local pollCurrentVote = 0

local isVoteActive
local hasAlreadyVoted = false
local isChangeAllowed = false

local finishTime

local cacheVoteNumber
local cacheTimer

addEvent("doShowPoll", true)
addEvent("doSendVote", true)
addEvent("doStopPoll", true)

addEventHandler("doShowPoll", root,
	function (pollData, pollOptions, pollTime)
		--clear the send vote cache
		cacheVoteNumber = ""
		--clear the bound keys table
		boundVoteKeys = {}
		--store the vote option names in the array nameFromVoteID
		nameFromVoteID = pollOptions
		--then build a reverse table
		voteIDFromName = {}

		pollPassPercentage = 100 / pollData.percentage

		finishTime = getTickCount() + pollTime
		pollCurrentVote = 0

	    for id, name in ipairs(nameFromVoteID) do
			voteIDFromName[name] = id
		end

		isChangeAllowed = pollData.allowchange
		
		--for each option, bind its key and create its label
		for index, option in ipairs(pollOptions) do
			--bind the number key and add it to the bound keys table
			local optionKey = tostring(index)
			bindKey(optionKey, "down", sendVote_bind)
			bindKey("num_"..optionKey, "down", sendVote_bind)
			table.insert(boundVoteKeys, optionKey)
		end

		--bind 0 keys if there are more than 9 options
		if #pollOptions > 9 then
			bindKey("0", "down", sendVote_bind)
			bindKey("num_0", "down", sendVote_bind)
			table.insert(boundVoteKeys, "0")
		end

		if isChangeAllowed then
			bindKey("backspace", "down", sendVote_bind)
		end
        
        -- Calculate GUI width
		pollTitle = pollData.title
		pollBoxRect = pollBoxRectDefault
		local width = math.max( pollBoxRect.width, dxGetTextWidth(pollTitle.."  Time left: 99", (screenX/1080) - 0.25, "default-bold", true) + 20 )
		for index, option in ipairs(pollOptions) do
			width = math.max(width, dxGetTextWidth(index .. ". " .. option, 1, "default-bold", true) + 20)
		end
		pollBoxRect.width = width
		pollBoxRect.x = screenX - width - 10

		--make poll options available to draw GUI
		pollOptionsText = pollOptions
		
		isVoteActive = true
	end
)

addEventHandler("doStopPoll", root,
	function ()
		isVoteActive = false
		hasAlreadyVoted = false

		for i, key in ipairs(boundVoteKeys) do
			unbindKey(key, "down", sendVote_bind)
			unbindKey("num_"..key, "down", sendVote_bind)
		end

		unbindKey("backspace", "down", sendVote_bind)
		
		pollVotes = nil
		pollMaxVoters = 0
		pollBoxRect = pollBoxRectDefault
	end
)

function sendVote_bind(key)
	if key ~= "backspace" then
		key = key:gsub('num_', '')
		if #nameFromVoteID < 10 then
			return sendVote(tonumber(key))
		else
			cacheVoteNumber = cacheVoteNumber..key
			if #cacheVoteNumber > 1 then
				if isTimer(cacheTimer) then
					killTimer(cacheTimer)
				end
				cacheVoteNumber = tonumber(cacheVoteNumber)
				if nameFromVoteID[cacheVoteNumber] then
					if cacheVoteNumber < 10 then
						return sendVote(cacheVoteNumber)
					else
						cacheTimer = setTimer(sendVote, 500, 1, cacheVoteNumber)
					end
					cacheVoteNumber = key
				else
					cacheVoteNumber = ""
				end
			else
				cacheTimer = setTimer(sendVote, 500, 1, tonumber(cacheVoteNumber))
			end
		end
	else
		return sendVote(-1)
	end
end

function sendVote(voteID)
	if not isVoteActive then
		return
	end

	--if option changing is not allowed, unbind the keys
	if not isChangeAllowed and voteID ~= -1 then
		for i, key in ipairs(boundVoteKeys) do
			unbindKey(key, "down", sendVote_bind)
			unbindKey("num_"..key, "down", sendVote_bind)
		end
	end
	
	--clear the send vote cache
	cacheVoteNumber = ""
	hasAlreadyVoted = voteID

	--send the vote to the server
	triggerServerEvent("onClientSendVote", localPlayer, voteID)
	
	pollCurrentVote = voteID

end
addEventHandler("doSendVote", root, sendVote)

addCommandHandler("vote",
	function (command, ...)
		--join all passed parameters separated by spaces
		local voteString = table.concat({...}, ' ')
		--try to get the voteID number
		local voteID = tonumber(voteString) or voteIDFromName[voteString]
		--if vote number is valid, send it
		if voteID and (nameFromVoteID[voteID] or voteID == -1) then
			sendVote(voteID)
		end
	end
)

addCommandHandler("cancelvote",
	function ()
		sendVote(-1)
	end
)


addEvent( "updateBars", true )
function updateBars(voteCount, maxVoters) 
	pollVotes = voteCount
	pollMaxVoters = maxVoters
end
addEventHandler("updateBars", root, updateBars)

function draw()
	if isVoteActive then
		dxDrawRectangle ( pollBoxRect.x, pollBoxRect.y, pollBoxRect.width, pollBoxRect.height, tocolor ( 0, 0, 50, 150 ), true ) -- Create transparent background
		
		-- Create outline
		dxDrawRectangle ( pollBoxRect.x, pollBoxRect.y, 5, pollBoxRect.height, tocolor ( 0, 0, 0, 200 ), true )
		dxDrawRectangle ( pollBoxRect.x, pollBoxRect.y, pollBoxRect.width, 5, tocolor ( 0, 0, 0, 200 ), true )
		dxDrawRectangle ( pollBoxRect.x + pollBoxRect.width - 5, pollBoxRect.y, 5, pollBoxRect.height, tocolor ( 0, 0, 0, 200 ), true )
		dxDrawRectangle ( pollBoxRect.x, pollBoxRect.y + pollBoxRect.height - 5, pollBoxRect.width, 5, tocolor ( 0, 0, 0, 200 ), true )
		
		local seconds = math.ceil( (finishTime - getTickCount()) / 1000 ) -- Calc time left in vote
		
		dxDrawText ( pollTitle.."  Time left: "..seconds, pollBoxRect.x + 10, pollBoxRect.y + 10, screenX, screenY, tocolor ( 255, 255, 255, 255 ), (screenX/1080) - 0.25, "default-bold", "left","top",false,false,true) -- Draw poll title
		
		for index, option in ipairs(pollOptionsText) do
			if pollCurrentVote == index then
				dxDrawText (index .. ". " .. option, pollBoxRect.x + 10, pollBoxRect.y + 15 + (40 * index), screenX, screenY, tocolor ( 255, 50, 50, 255 ), 1, "default-bold", "left","top",false,false,true)
			else
				dxDrawText (index .. ". " .. option, pollBoxRect.x + 10, pollBoxRect.y + 15 + (40 * index), screenX, screenY, tocolor ( 255, 255, 255, 255 ), 1, "default-bold", "left","top",false,false,true)
			end

			dxDrawRectangle (pollBoxRect.x + 10, pollBoxRect.y + 35 + (40 * index), pollBoxRect.width - 20, 10, tocolor ( 255, 255, 255, 255 ), true)
			if pollMaxVoters ~= 0 then
				dxDrawRectangle (pollBoxRect.x + 12, pollBoxRect.y + 37 + (40 * index), (pollBoxRect.width - 24) * ((pollVotes[index] / pollMaxVoters)*pollPassPercentage), 6, tocolor ( (pollVotes[index] / pollMaxVoters)*355, 0, 0, 255), true)
			end
		end
	end
end
addEventHandler("onClientRender", root, draw)
