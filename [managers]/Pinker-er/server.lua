local chatCycle = 1

local spamMessages = {
	"Remember to use /rate (0-10)! Low rated maps will be considered for removal.",
	"If you have any maps you would like added, contact @the123robot on Discord.",
	"Discuss the server in the #mta-general chatroom on Discord: discord.gg/gt2V8An",
	"Please report players acting inappropriately by typing /report and selecting 'Player Complaints'. These will be reviewed regularly.",
	"Not interested in playing the current map? Why not look for hidden packages around San Andreas? Press F9 for more information.",
	"Press F9 to view information about server commands and resources.",
	"Remember to /register if you haven't already! This ensures all of your data and settings are fully saved.",
	"Type /motd to view server rules and the latest server changelogs.",
	"Bored with the current map or died early in a DD? Type /2048 for another way to have fun while you wait.",
	"You can use the /report command to inform us of issues with maps or resources, suggest new ideas, or report troublesome players.",
	"If you're able, you can donate to help support the server and its upkeep through my Streamlabs: streamlabs.com/123robot"
}

-- https://wiki.multitheftauto.com/wiki/GetVehicleColor

function getRandomColor()
	local index = 1
	local random = math.random(1690)
	for i, v in pairs(COLORS) do
		if index == random then
			return v
		end
		index = index + 1
	end
end

function getCarColorsFromColorName(hex)
	-- local hex = COLORS[name] or nil
	if not hex then
		return false
	end
	r1,g1,b1 = getColorFromString(hex)
	r3 = math.floor(r1 / 2)
	g3 = math.floor(g1 / 2)
	b3 = math.floor(b1 / 2)
	r2 = r1 + math.floor((255 - r1)/2)
	g2 = g1 + math.floor((255 - g1)/2)
	b2 = b1 + math.floor((255 - b1)/2)
	return {r1,g1,b1,r2,g2,b2,r3,g3,b3,0,0,0}
end

function changeColor (playerSource, commandName, playerToPaint, targetColor)
	if (not playerToPaint and not targetColor) then
		local randomSelfPaintColor = getRandomColor()
		paintPlayer(playerSource, getCarColorsFromColorName(randomSelfPaintColor))
		return
	end
	
	local playerUnit = findPlayer(playerToPaint)
	if not playerUnit then
		targetColor = playerToPaint
		playerToPaint = playerSource
		playerUnit = playerSource
	end
	if (targetColor) then
		local random = math.random(1690)
		local success = false
		for i, v in pairs(COLORS) do
			if i == targetColor then
				targetColor = v
				success = true
				break
			end
		end
		if (not success) then
			outputChatBox("Not found: " .. targetColor, playerSource)
			return
		end
		paintPlayer(playerUnit, getCarColorsFromColorName(targetColor))
		return
	else
		local randomSelfPaintColor = getRandomColor()
		paintPlayer(playerUnit, getCarColorsFromColorName(randomSelfPaintColor))
		return
	end
end
addCommandHandler ( "colorify", changeColor)

function findPlayer(player_input)
	alivePlayers = getAlivePlayers ()
	if ( alivePlayers ) then
		for playerKey, playerValue in ipairs(alivePlayers) do
			if string.find(RemoveHEXColorCode(getPlayerName(playerValue)), player_input) then  -- Find first occurrence matching input
				return playerValue
			end
		end
	end
end

function paintPlayer(player, color)
	if not player then
		return
	end
	if getPlayerTeam(player) then
		return
	end
	local vehicleToPaint = getPedOccupiedVehicle(player)
	if vehicleToPaint then
		setVehicleColor(vehicleToPaint, unpack(color))
	end
end

function RemoveHEXColorCode( name )
	return name:gsub( '#%x%x%x%x%x%x', '' ) or name
end

addEventHandler ( "onResourceStart", resourceRoot,  function()
	setTimer(RandomPink, 10000, 0)
	setTimer(SpamChat, 240000, 0)
end
)

function RandomPink()
	if getPlayerCount() > 0 then
		if math.random(100) == 50 then
			local randomSelfPaintColor = getRandomColor()
			paintPlayer(getRandomPlayer(), getCarColorsFromColorName(randomSelfPaintColor))
		end
	end
end

function SpamChat()
	if chatCycle <= #spamMessages then
		outputChatBox(spamMessages[chatCycle], getRootElement(), 23, 198, 29) -- #17C61D
		chatCycle = chatCycle + 1
	else
		chatCycle = 1
	end
end

-- Fix for server built-in me command handling player color tags
addEventHandler ( "onPlayerChat", root, function ( message, message_type )
	if ( message_type == 1 ) then  -- type 1 is reserved for /me messages
		cancelEvent ()
		outputChatBox("* " .. RemoveHEXColorCode(getPlayerName(source)) .. " " .. message, root, 162, 25, 255) -- #A219FF
	end
end )
